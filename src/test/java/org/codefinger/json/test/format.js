var i = {
	"intArrayValue" : [ 0, 112300, 2, 3 ],
	"floatArrayValue" : [ 0.0, -1231000.0, -112.0 ],
	"doubleArrayValue" : [ 0.0, -45650.0, 2.0 ],
	"shortArrayValue" : [ 0, 1, 2, 3 ],
	"longArrayValue" : [ 0, 1233, 12000, 123 ],
	"byteArrayValue" : "6L+Z6YeM5piv5LiA5q61QmFzZTY05YaF5a6577yM5aaC5p6c5oKo5bey57uP55yL5Yiw5q2k5a6M5pW05YaF5a6577yI5Lul4oCc44CC4oCd57uT5bC+77yJ77yM6K+B5piO5oKo6Kej56CB5oiQ5Yqf5LqG44CC",
	"charArrayValue" : "中文Hello三月",
	"booleanArrayValue" : [ false, false, true, false ],
	"intValue" : -1123123,
	"floatValue" : 1123123.0,
	"doubleValue" : 123123.0,
	"shortValue" : 123,
	"longValue" : 12312312312,
	"byteValue" : 0,
	"charValue" : "B",
	"booleanValue" : true,
	"pkgIntValue" : 1,
	"pkgFloatValue" : 1.0,
	"pkgDoubleValue" : 1.0,
	"pkgShortValue" : 1,
	"pkgLongValue" : 1,
	"pkgByteValue" : 1,
	"pkgCharValue" : "X",
	"pkgBooleanValue" : false,
	"stringValue" : "I 三月 love 四月 codefinger 五月",
	"dateValue" : 1438425411860,
	"bigDecimalValue" : 1123123123123123.123123123,
	"bigIntegerValue" : 1123123123123123,
	"pkgBooleanArrayValue" : [ false, true, false, false ],
	"stringArrayValue" : [ "三月", "\r\n\t\f\b\\/", "codefinger\"'", "Hello" ],
	"dateArrayValue" : [ 1438425412860, 1438425512860, 14384255128607 ],
	"bigDecimalArrayValue" : [ 123.123, 456.45745645, 123123123123 ],
	"bigIntegerArrayValue" : [ 1, 1111111111111111111111,
			11111111111111111111111, 111 ],
	"gValue" : {
		"Hello" : [ [ {
			"123456" : [ {
				"123456.123456" : {
					"Sat Aug 01 18:36:52 CST 2015" : "A"
				}
			}, {
				"123456.12345789" : {
					"Sat Aug 01 18:36:52 CST 2015" : "B"
				}
			} ]
		} ] ]
	},
	"jsonEnum" : "Codefinger",
	"jsonEnums" : [ "Hello", "World", "MyName", "Is", "Codefinger", "Please",
			"Remember", "Me" ]
};