package org.codefinger.json.serializer;

public class SerializerForBoolean implements JSONSerializer {

	public static SerializerForBoolean	INSTANCE	= new SerializerForBoolean();

	private SerializerForBoolean() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.writeBoolean((Boolean) value);
	}
}
