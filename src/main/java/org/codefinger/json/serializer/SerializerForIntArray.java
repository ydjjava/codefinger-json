package org.codefinger.json.serializer;

public class SerializerForIntArray implements JSONSerializer {

	public static SerializerForIntArray	INSTANCE	= new SerializerForIntArray();

	private SerializerForIntArray() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		int[] i = (int[]) value;
		writer.write('[');
		int length = i.length;
		if (length > 0) {
			writer.writeInt(i[0]);
			for (int j = 1; j < length; j++) {
				writer.write(',');
				writer.writeInt(i[j]);
			}
		}
		writer.write(']');
	}
}
