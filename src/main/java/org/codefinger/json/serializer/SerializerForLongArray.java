package org.codefinger.json.serializer;

public class SerializerForLongArray implements JSONSerializer {

	public static SerializerForLongArray	INSTANCE	= new SerializerForLongArray();

	private SerializerForLongArray() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		long[] i = (long[]) value;
		writer.write('[');
		int length = i.length;
		if (length > 0) {
			writer.writeLong(i[0]);
			for (int j = 1; j < length; j++) {
				writer.write(',');
				writer.writeLong(i[j]);
			}
		}
		writer.write(']');
	}
}
