package org.codefinger.json.serializer;

public class SerializerForShortArray implements JSONSerializer {

	public static SerializerForShortArray	INSTANCE	= new SerializerForShortArray();

	private SerializerForShortArray() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		short[] i = (short[]) value;
		writer.write('[');
		int length = i.length;
		if (length > 0) {
			writer.writeInt(i[0]);
			for (int j = 1; j < length; j++) {
				writer.write(',');
				writer.writeInt(i[j]);
			}
		}
		writer.write(']');
	}
}
