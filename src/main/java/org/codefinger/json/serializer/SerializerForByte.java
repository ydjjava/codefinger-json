package org.codefinger.json.serializer;

public class SerializerForByte implements JSONSerializer {

	public static SerializerForByte	INSTANCE	= new SerializerForByte();

	private SerializerForByte() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.writeInt((Byte) value);
	}

}
