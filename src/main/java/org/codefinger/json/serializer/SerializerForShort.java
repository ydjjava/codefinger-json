package org.codefinger.json.serializer;

public class SerializerForShort implements JSONSerializer {

	public static SerializerForShort	INSTANCE	= new SerializerForShort();

	private SerializerForShort() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.writeInt((Short) value);
	}

}
