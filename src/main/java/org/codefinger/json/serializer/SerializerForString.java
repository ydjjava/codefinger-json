package org.codefinger.json.serializer;

public class SerializerForString implements JSONSerializer {

	public static SerializerForString	INSTANCE	= new SerializerForString();

	private SerializerForString() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.writeString(value.toString());
	}

}
