package org.codefinger.json.serializer;

import java.util.Date;

public class SerializerForDate implements JSONSerializer {

	public static SerializerForDate	INSTANCE	= new SerializerForDate();

	private SerializerForDate() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.writeLong(((Date) value).getTime());
	}

}
