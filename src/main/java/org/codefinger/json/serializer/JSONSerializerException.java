package org.codefinger.json.serializer;

public class JSONSerializerException extends RuntimeException {

	private static final long	serialVersionUID	= 1066406251633191014L;

	public JSONSerializerException(String message, Throwable cause) {
		super(message, cause);
	}

	public JSONSerializerException(String message) {
		super(message);
	}

	public JSONSerializerException(Throwable cause) {
		super(cause);
	}

}
