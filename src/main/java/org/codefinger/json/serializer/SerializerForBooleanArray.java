package org.codefinger.json.serializer;

public class SerializerForBooleanArray implements JSONSerializer {

	public static SerializerForBooleanArray	INSTANCE	= new SerializerForBooleanArray();

	private SerializerForBooleanArray() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		boolean[] i = (boolean[]) value;
		writer.write('[');
		int length = i.length;
		if (length > 0) {
			writer.writeBoolean(i[0]);
			for (int j = 1; j < length; j++) {
				writer.write(',');
				writer.writeBoolean(i[j]);
			}
		}
		writer.write(']');
	}
}
