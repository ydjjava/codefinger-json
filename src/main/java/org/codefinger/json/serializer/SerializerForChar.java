package org.codefinger.json.serializer;

public class SerializerForChar implements JSONSerializer {

	public static SerializerForChar	INSTANCE	= new SerializerForChar();

	private SerializerForChar() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.write('"');
		writer.write((Character) value);
		writer.write('"');
	}

}
