package org.codefinger.json;

public enum JSONType {

	Object, Array, Number, String, Boolean, Date

}
