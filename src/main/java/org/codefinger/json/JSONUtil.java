package org.codefinger.json;

import java.io.Writer;
import java.lang.reflect.Type;

import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONParser;
import org.codefinger.json.serializer.JSONSerializerFactory;
import org.codefinger.json.serializer.SerializerWriter;

public class JSONUtil {

	private JSONUtil() {

	}

	public static String toJSONString(Object object) {
		SerializerWriter writer = new SerializerWriter();
		JSONSerializerFactory.write(writer, object);
		String result = writer.toString();
		writer.close();
		return result;
	}

	public static void writeJSONString(Writer writer, Object object) {
		SerializerWriter serializerWriter = new SerializerWriter(writer);
		JSONSerializerFactory.write(serializerWriter, object);
		serializerWriter.close();
	}

	public static <T> T parse(char[] chars, Type type) {
		return new JSONParser(new JSONLexer(chars)).parse(type);
	}

	public static <T> T parse(char[] chars, Class<T> type) {
		return new JSONParser(new JSONLexer(chars)).parse(type);
	}

	public static <T> T parse(String chars, Type type) {
		return new JSONParser(new JSONLexer(chars)).parse(type);
	}

	public static <T> T parse(String chars, Class<T> type) {
		return new JSONParser(new JSONLexer(chars)).parse(type);
	}

	public static JSONObject parseObject(char[] chars) {
		return new JSONParser(new JSONLexer(chars)).parse(JSONObject.class);
	}

	public static JSONObject parseObject(String chars) {
		return new JSONParser(new JSONLexer(chars)).parse(JSONObject.class);
	}

	public static JSONArray parseArray(char[] chars) {
		return new JSONParser(new JSONLexer(chars)).parse(JSONArray.class);
	}

	public static JSONArray parseArray(String chars) {
		return new JSONParser(new JSONLexer(chars)).parse(JSONArray.class);
	}

	@SuppressWarnings("unchecked")
	public static <T> T toJavaObject(Object value, Class<T> clazz) {
		return (T) JSONParser.parseTo(value, clazz);
	}

	@SuppressWarnings("unchecked")
	public static <T> T toJavaObject(Object value, Type type) {
		return (T) JSONParser.parseTo(value, type);
	}

}
