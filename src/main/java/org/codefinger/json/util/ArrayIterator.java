package org.codefinger.json.util;

import java.util.Iterator;

/**
 * 
 * <h1>数组迭代器</h1>
 *
 * @author Dcode
 *
 * @param <T>
 */
public class ArrayIterator<T> implements Iterator<T> {

	private final T[]	array;

	private final int	length;

	private int			index;

	public ArrayIterator(T[] array) {
		if (array == null) {
			this.array = null;
			this.length = 0;
		} else {
			this.array = array;
			this.length = array.length;
		}
	}

	@Override
	public void remove() {
		index++;
	}

	@Override
	public boolean hasNext() {
		return index < length;
	}

	@Override
	public T next() {
		return array[index++];
	}

	/**
	 * 重置迭代器
	 */
	public void reset() {
		index = 0;
	}

	public int length() {
		return length;
	}

	public void copyArray(T[] array) {
		System.arraycopy(this.array, 0, array, 0, array.length);
	}
}
