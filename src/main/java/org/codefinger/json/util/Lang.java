package org.codefinger.json.util;

import java.util.concurrent.atomic.AtomicLong;

public class Lang {

	private Lang() {

	}

	private static final AtomicLong	LONG_ADDER	= new AtomicLong();

	public static long getLongAdder() {
		return LONG_ADDER.incrementAndGet();
	}

	public static RuntimeException makeThrow(String formatMsg, Object... formatArg) {
		RuntimeException runtimeException = new RuntimeException(String.format(formatMsg, formatArg));
		StackTraceElement[] elements = runtimeException.getStackTrace();
		int length = elements.length - 1;
		StackTraceElement[] newElements = new StackTraceElement[length];
		System.arraycopy(elements, 1, newElements, 0, length);
		runtimeException.setStackTrace(newElements);
		throw runtimeException;
	}

	public static RuntimeException wrapThrow(Throwable e, String formatMsg, Object... formatArg) {
		RuntimeException runtimeException = new RuntimeException(String.format(formatMsg, formatArg), e);
		StackTraceElement[] elements = runtimeException.getStackTrace();
		int length = elements.length - 1;
		StackTraceElement[] newElements = new StackTraceElement[length];
		System.arraycopy(elements, 1, newElements, 0, length);
		runtimeException.setStackTrace(newElements);
		throw runtimeException;
	}

	public static String joinString(Object... objects) {
		StringBuilder builder = new StringBuilder();
		for (Object object : objects) {
			builder.append(object);
		}
		return builder.toString();
	}

}
