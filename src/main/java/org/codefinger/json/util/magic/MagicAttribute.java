package org.codefinger.json.util.magic;

import java.lang.reflect.Field;

public interface MagicAttribute {

	Field getField();

	String getFieldName();

	void setValue(Object object, Object value);

	Object getValue(Object object);

}
