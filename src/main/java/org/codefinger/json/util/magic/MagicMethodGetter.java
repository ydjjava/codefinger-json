package org.codefinger.json.util.magic;

public class MagicMethodGetter implements MagicGetter {

	private MagicMethod	magicMethod;

	MagicMethodGetter(MagicMethod magicMethod) {
		this.magicMethod = magicMethod;
	}

	@Override
	public Object getValue(Object object) {
		return magicMethod.invoke(object);
	}

}
