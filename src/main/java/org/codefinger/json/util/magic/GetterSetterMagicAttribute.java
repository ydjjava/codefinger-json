package org.codefinger.json.util.magic;

import java.lang.reflect.Field;

public class GetterSetterMagicAttribute implements MagicAttribute {

	private Field		field;

	private String		fieldName;

	private MagicGetter	magicGetter;

	private MagicSetter	magicSetter;

	GetterSetterMagicAttribute(Field field, MagicGetter magicGetter, MagicSetter magicSetter) {
		super();
		this.fieldName = field.getName();
		this.field = field;
		this.magicGetter = magicGetter;
		this.magicSetter = magicSetter;
	}

	@Override
	public Field getField() {
		return field;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Override
	public void setValue(Object object, Object value) {
		magicSetter.setValue(object, value);
	}

	@Override
	public Object getValue(Object object) {
		return magicGetter.getValue(object);
	}
}
