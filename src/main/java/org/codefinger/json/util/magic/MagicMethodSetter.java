package org.codefinger.json.util.magic;

public class MagicMethodSetter implements MagicSetter {

	private MagicMethod	magicMethod;

	MagicMethodSetter(MagicMethod magicMethod) {
		this.magicMethod = magicMethod;
	}

	@Override
	public void setValue(Object object, Object value) {
		magicMethod.invoke(object, value);
	}

}
