package org.codefinger.json.util.magic;

public interface MagicGetter {

	Object getValue(Object object);

}
