package org.codefinger.json.util.magic;

public interface MagicSetter {

	void setValue(Object object, Object value);

}
