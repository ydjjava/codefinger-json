package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.TypeConverter;

public class BigDecimalConverter implements TypeConverter {

	public static final BigDecimalConverter	INSTANCE	= new BigDecimalConverter();

	private BigDecimalConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return ((BigDecimal) value).intValue();
	}

	@Override
	public Byte castToByte(Object value) {
		return ((BigDecimal) value).byteValue();
	}

	@Override
	public Character castToChar(Object value) {
		return (char) ((BigDecimal) value).intValue();
	}

	@Override
	public Short castToShort(Object value) {
		return ((BigDecimal) value).shortValue();
	}

	@Override
	public Long castToLong(Object value) {
		return ((BigDecimal) value).longValue();
	}

	@Override
	public Float castToFloat(Object value) {
		return ((BigDecimal) value).floatValue();
	}

	@Override
	public Double castToDouble(Object value) {
		return ((BigDecimal) value).doubleValue();
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return ((BigDecimal) value).intValue() == 0 ? false : true;
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return (BigDecimal) value;
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return ((BigDecimal) value).toBigInteger();
	}

	@Override
	public Date castToDate(Object value) {
		return new Date(((BigDecimal) value).longValue());
	}

}
