package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.util.type.TypeConverter;

public class JSONBaseConverter implements TypeConverter {

	public static final JSONBaseConverter	INSTANCE	= new JSONBaseConverter();

	private JSONBaseConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return ((JSONBase) value).intValue();
	}

	@Override
	public Byte castToByte(Object value) {
		return ((JSONBase) value).byteValue();
	}

	@Override
	public Character castToChar(Object value) {
		return ((JSONBase) value).charValue();
	}

	@Override
	public Short castToShort(Object value) {
		return ((JSONBase) value).shortValue();
	}

	@Override
	public Long castToLong(Object value) {
		return ((JSONBase) value).longValue();
	}

	@Override
	public Float castToFloat(Object value) {
		return ((JSONBase) value).floatValue();
	}

	@Override
	public Double castToDouble(Object value) {
		return ((JSONBase) value).doubleValue();
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return ((JSONBase) value).booleanValue();
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return ((JSONBase) value).bigDecimalValue();
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return ((JSONBase) value).bigIntegerValue();
	}

	@Override
	public Date castToDate(Object value) {
		return ((JSONBase) value).dateValue();
	}

}
