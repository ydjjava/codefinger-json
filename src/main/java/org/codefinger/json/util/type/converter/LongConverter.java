package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.TypeConverter;

public class LongConverter implements TypeConverter {

	public static final LongConverter	INSTANCE	= new LongConverter();

	private LongConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return ((Long) value).intValue();
	}

	@Override
	public Byte castToByte(Object value) {
		return ((Long) value).byteValue();
	}

	@Override
	public Character castToChar(Object value) {
		return (char) ((Long) value).intValue();
	}

	@Override
	public Short castToShort(Object value) {
		return ((Long) value).shortValue();
	}

	@Override
	public Long castToLong(Object value) {
		return (Long) value;
	}

	@Override
	public Float castToFloat(Object value) {
		return ((Long) value).floatValue();
	}

	@Override
	public Double castToDouble(Object value) {
		return ((Long) value).doubleValue();
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return (Long) value == 0 ? false : true;
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return new BigDecimal(value.toString());
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return BigInteger.valueOf(((Long) value));
	}

	@Override
	public Date castToDate(Object value) {
		return new Date(((Long) value).longValue());
	}

}
