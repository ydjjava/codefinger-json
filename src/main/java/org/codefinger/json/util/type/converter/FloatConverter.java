package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.TypeConverter;

public class FloatConverter implements TypeConverter {

	public static final FloatConverter	INSTANCE	= new FloatConverter();

	private FloatConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return ((Float) value).intValue();
	}

	@Override
	public Byte castToByte(Object value) {
		return ((Float) value).byteValue();
	}

	@Override
	public Character castToChar(Object value) {
		return (char) ((Float) value).intValue();
	}

	@Override
	public Short castToShort(Object value) {
		return ((Float) value).shortValue();
	}

	@Override
	public Long castToLong(Object value) {
		return ((Float) value).longValue();
	}

	@Override
	public Float castToFloat(Object value) {
		return (Float) value;
	}

	@Override
	public Double castToDouble(Object value) {
		return ((Float) value).doubleValue();
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return (Float) value == 0 ? false : true;
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return new BigDecimal(value.toString());
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return BigInteger.valueOf(((Float) value).longValue());
	}

	@Override
	public Date castToDate(Object value) {
		return new Date(((Float) value).longValue());
	}

}
