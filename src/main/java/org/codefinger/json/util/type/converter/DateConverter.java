package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.DateUtil;
import org.codefinger.json.util.type.TypeConverter;

public class DateConverter implements TypeConverter {

	public static final DateConverter	INSTANCE	= new DateConverter();

	private DateConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return null;
	}

	@Override
	public Byte castToByte(Object value) {
		return null;
	}

	@Override
	public Character castToChar(Object value) {
		return null;
	}

	@Override
	public Short castToShort(Object value) {
		return null;
	}

	@Override
	public Long castToLong(Object value) {
		return ((Date) value).getTime();
	}

	@Override
	public Float castToFloat(Object value) {
		return (float) ((Date) value).getTime();
	}

	@Override
	public Double castToDouble(Object value) {
		return (double) ((Date) value).getTime();
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return false;
	}

	@Override
	public String castToString(Object value) {
		return DateUtil.formatSimple((Date) value);
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return new BigDecimal(((Date) value).getTime());
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return BigInteger.valueOf(((Date) value).getTime());
	}

	@Override
	public Date castToDate(Object value) {
		return (Date) value;
	}

}
