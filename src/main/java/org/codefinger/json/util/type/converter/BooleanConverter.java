package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.TypeConverter;

public class BooleanConverter implements TypeConverter {

	public static final BooleanConverter	INSTANCE	= new BooleanConverter();

	private BooleanConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return (Boolean) value ? 1 : 0;
	}

	@Override
	public Byte castToByte(Object value) {
		return (byte) ((Boolean) value ? 1 : 0);
	}

	@Override
	public Character castToChar(Object value) {
		return (char) ((Boolean) value ? 1 : 0);
	}

	@Override
	public Short castToShort(Object value) {
		return (short) ((Boolean) value ? 1 : 0);
	}

	@Override
	public Long castToLong(Object value) {
		return (Boolean) value ? 1L : 0L;
	}

	@Override
	public Float castToFloat(Object value) {
		return (Boolean) value ? 1F : 0F;
	}

	@Override
	public Double castToDouble(Object value) {
		return (Boolean) value ? 1.0 : 0;
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return (Boolean) value;
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return (Boolean) value ? new BigDecimal("1") : new BigDecimal("0");
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return (Boolean) value ? new BigInteger("1") : new BigInteger("0");
	}

	@Override
	public Date castToDate(Object value) {
		return null;
	}

}
