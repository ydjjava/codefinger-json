package org.codefinger.json.parser;

public class JSONSyntaxException extends RuntimeException {

	private static final long	serialVersionUID	= -8490789818277198204L;

	public JSONSyntaxException() {
		super();
	}

	public JSONSyntaxException(String message) {
		super(message);
	}

}
