package org.codefinger.json.parser;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;

public interface JSONObjectDeserializer {

	JSONArrayDeserializer createArrayDeserializer();

	JSONObjectDeserializer createObjectDeserializer();

	void setFieldName(JSONLexer jsonLexer);

	void setFieldName(String fieldName);

	void setValue(Object value);

	void setStringValue(JSONLexer jsonLexer);

	void setIntValue(String value);

	void setFloatValue(String value);

	void setFalseValue();

	void setTrueValue();

	Object getDeserializedValue();

	void setValue(Integer value);

	void setValue(Double value);

	void setValue(Float value);

	void setValue(Long value);

	void setValue(Short value);

	void setValue(Byte value);

	void setValue(Boolean value);

	void setValue(Character value);

	void setValue(Date value);

	void setValue(BigDecimal value);

	void setValue(BigInteger value);

	void setValue(JSONBase value);

	void setValue(String value);

}
