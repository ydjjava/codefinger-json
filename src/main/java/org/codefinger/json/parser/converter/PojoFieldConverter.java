package org.codefinger.json.parser.converter;

import org.codefinger.json.parser.JSONConverter;
import org.codefinger.json.parser.JSONObjectDeserializer;
import org.codefinger.json.util.Lang;
import org.codefinger.json.util.magic.MagicAttribute;

public class PojoFieldConverter {

	private MagicAttribute	magicAttribute;

	private JSONConverter	converter;

	public PojoFieldConverter(MagicAttribute magicAttribute, JSONConverter converter) {
		super();
		this.magicAttribute = magicAttribute;
		this.converter = converter;
	}

	public void setItem(Object value, JSONObjectDeserializer deserializer) {
		switch (converter.jsonConverterType()) {
		case Base:
			converter.setItemValue(magicAttribute.getValue(value), deserializer);
			return;
		case Object:
			deserializer.setValue(converter.convert(//
					magicAttribute.getValue(value), deserializer.createObjectDeserializer()));
			return;
		case Array:
			deserializer.setValue(converter.convert(//
					magicAttribute.getValue(value), deserializer.createArrayDeserializer()));
			return;
		default:
			throw Lang.makeThrow("It is impossible!");
		}
	}
}
