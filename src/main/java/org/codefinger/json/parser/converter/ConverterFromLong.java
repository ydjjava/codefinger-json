package org.codefinger.json.parser.converter;

import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONConverter;
import org.codefinger.json.parser.JSONConverterType;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ConverterFromLong implements JSONConverter {

	public static final ConverterFromLong	INSTANCE	= new ConverterFromLong();

	private ConverterFromLong() {

	}

	@Override
	public Object convert(Object value, JSONDeserializer deserializer) {
		return deserializer.value((Long) value);
	}

	@Override
	public Object convert(Object value, JSONObjectDeserializer objectDeserializer) {
		return null;
	}

	@Override
	public Object convert(Object value, JSONArrayDeserializer arrayDeserializer) {
		return null;
	}

	@Override
	public void setItemValue(Object value, JSONObjectDeserializer objectDeserializer) {
		objectDeserializer.setValue((Long) value);
	}

	@Override
	public void addItemValue(Object value, JSONArrayDeserializer arrayDeserializer) {
		arrayDeserializer.addValue((Long) value);
	}

	@Override
	public JSONConverterType jsonConverterType() {
		return JSONConverterType.Base;
	}

}
