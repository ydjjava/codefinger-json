package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForDoubleArray implements JSONDeserializer {

	public static final DeserializerForDoubleArray	INSTANCE	= new DeserializerForDoubleArray();

	private DeserializerForDoubleArray() {

	}

	@Override
	public Object stringValue(String value) {
		return new double[] { Double.parseDouble(value) };
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return new double[] { Double.parseDouble(jsonLexer.getOriginalString()) };
	}

	@Override
	public Object intValue(String value) {
		return new double[] { Double.parseDouble(value) };
	}

	@Override
	public Object floatValue(String value) {
		return new double[] { Double.parseDouble(value) };
	}

	@Override
	public Object falseValue() {
		return new double[] { 0 };
	}

	@Override
	public Object trueValue() {
		return new double[] { 1 };
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForDoubleArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return new double[] { value.doubleValue() };
	}

	@Override
	public Object value(Double value) {
		return new double[] { value.doubleValue() };
	}

	@Override
	public Object value(Float value) {
		return new double[] { value.doubleValue() };
	}

	@Override
	public Object value(Long value) {
		return new double[] { value.doubleValue() };
	}

	@Override
	public Object value(Short value) {
		return new double[] { value.doubleValue() };
	}

	@Override
	public Object value(Byte value) {
		return new double[] { value.doubleValue() };
	}

	@Override
	public Object value(Boolean value) {
		return new double[] { value ? 1 : 0 };
	}

	@Override
	public Object value(Character value) {
		return new double[] { value.charValue() };
	}

	@Override
	public Object value(Date value) {
		return new double[] { value.getTime() };
	}

	@Override
	public Object value(BigDecimal value) {
		return new double[] { value.doubleValue() };
	}

	@Override
	public Object value(BigInteger value) {
		return new double[] { value.doubleValue() };
	}

	@Override
	public Object value(JSONBase value) {
		return new double[] { value.doubleValue() };
	}
}
