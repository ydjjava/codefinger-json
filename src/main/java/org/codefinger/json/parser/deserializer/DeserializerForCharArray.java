package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForCharArray implements JSONDeserializer {

	public static final DeserializerForCharArray	INSTANCE	= new DeserializerForCharArray();

	private DeserializerForCharArray() {

	}

	@Override
	public Object stringValue(String value) {
		return value.toCharArray();
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return jsonLexer.getJSONString().toCharArray();
	}

	@Override
	public Object intValue(String value) {
		return value.toCharArray();
	}

	@Override
	public Object floatValue(String value) {
		return value.toCharArray();
	}

	@Override
	public Object falseValue() {
		return new char[] { 'f', 'a', 'l', 's', 'e' };
	}

	@Override
	public Object trueValue() {
		return new char[] { 't', 'r', 'u', 'e' };
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForCharArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return new char[] { (char) value.intValue() };
	}

	@Override
	public Object value(Double value) {
		return new char[] { (char) value.intValue() };
	}

	@Override
	public Object value(Float value) {
		return new char[] { (char) value.intValue() };
	}

	@Override
	public Object value(Long value) {
		return new char[] { (char) value.intValue() };
	}

	@Override
	public Object value(Short value) {
		return new char[] { (char) value.intValue() };
	}

	@Override
	public Object value(Byte value) {
		return new char[] { (char) value.intValue() };
	}

	@Override
	public Object value(Boolean value) {
		return new char[] { value ? (char) 1 : 0 };
	}

	@Override
	public Object value(Character value) {
		return new char[] { value };
	}

	@Override
	public Object value(Date value) {
		return new char[] { (char) value.getTime() };
	}

	@Override
	public Object value(BigDecimal value) {
		return new char[] { (char) value.intValue() };
	}

	@Override
	public Object value(BigInteger value) {
		return new char[] { (char) value.intValue() };
	}

	@Override
	public Object value(JSONBase value) {
		return new char[] { (char) value.intValue().intValue() };
	}

}
