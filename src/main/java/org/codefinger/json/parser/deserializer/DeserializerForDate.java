package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;
import org.codefinger.json.util.type.DateUtil;

public class DeserializerForDate implements JSONDeserializer {

	public static final DeserializerForDate	INSTANCE	= new DeserializerForDate();

	private DeserializerForDate() {

	}

	@Override
	public Object stringValue(String value) {
		return DateUtil.parseDate(value);
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return DateUtil.parseDate(jsonLexer.getOriginalString());
	}

	@Override
	public Object intValue(String value) {
		return new Date(Long.parseLong(value));
	}

	@Override
	public Object floatValue(String value) {
		return new Date(new BigInteger(value).longValue());
	}

	@Override
	public Object falseValue() {
		return null;
	}

	@Override
	public Object trueValue() {
		return null;
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return new Date(value.longValue());
	}

	@Override
	public Object value(Double value) {
		return new Date(value.longValue());
	}

	@Override
	public Object value(Float value) {
		return new Date(value.longValue());
	}

	@Override
	public Object value(Long value) {
		return new Date(value.longValue());
	}

	@Override
	public Object value(Short value) {
		return new Date(value.longValue());
	}

	@Override
	public Object value(Byte value) {
		return new Date(value.longValue());
	}

	@Override
	public Object value(Boolean value) {
		return null;
	}

	@Override
	public Object value(Character value) {
		return null;
	}

	@Override
	public Object value(Date value) {
		return null;
	}

	@Override
	public Object value(BigDecimal value) {
		return new Date(value.longValue());
	}

	@Override
	public Object value(BigInteger value) {
		return new Date(value.longValue());
	}

	@Override
	public Object value(JSONBase value) {
		return new Date(value.longValue());
	}

}
