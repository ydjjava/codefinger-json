package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;
import org.codefinger.json.util.magic.MagicAttribute;

public class PojoFieldDeserializer {

	private MagicAttribute		magicAttribute;

	private JSONDeserializer	deserializer;

	public PojoFieldDeserializer(MagicAttribute magicAttribute, JSONDeserializer deserializer) {
		super();
		this.magicAttribute = magicAttribute;
		this.deserializer = deserializer;
	}

	public JSONArrayDeserializer createArrayDeserializer() {
		return deserializer.createArrayDeserializer();
	}

	public JSONObjectDeserializer createObjectDeserializer() {
		return deserializer.createObjectDeserializer();
	}

	public void setValue(Object target, Object value) {
		magicAttribute.setValue(target, value);
	}

	public void setStringValue(Object target, JSONLexer jsonLexer) {
		magicAttribute.setValue(target, deserializer.stringValue(jsonLexer));
	}

	public void setIntValue(Object target, String value) {
		magicAttribute.setValue(target, deserializer.intValue(value));
	}

	public void setFloatValue(Object target, String value) {
		magicAttribute.setValue(target, deserializer.floatValue(value));
	}

	public void setFalseValue(Object target) {
		magicAttribute.setValue(target, deserializer.falseValue());
	}

	public void setTrueValue(Object target) {
		magicAttribute.setValue(target, deserializer.trueValue());
	}

	public void value(Object target, Integer value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, Double value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, Float value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, Long value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, Short value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, Byte value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, Boolean value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, Character value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, Date value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, BigDecimal value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, BigInteger value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, JSONBase value) {
		magicAttribute.setValue(target, deserializer.value(value));
	}

	public void value(Object target, String value) {
		magicAttribute.setValue(target, deserializer.stringValue(value));
	}

}
