package org.codefinger.json.parser.deserializer;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;
import org.codefinger.json.parser.JSONParser;
import org.codefinger.json.util.IdentityCache;
import org.codefinger.json.util.IdentityCache.ValueBuilder;
import org.codefinger.json.util.magic.MagicConstructor;
import org.codefinger.json.util.type.Generic;

public class DeserializerForCollection implements JSONDeserializer {

	private static final IdentityCache<Type, InitializationDevice>	INIT_MAP	= new IdentityCache<Type, InitializationDevice>(new InitializationDeviceBuilder());

	private Type													type;

	public DeserializerForCollection(Type type) {
		this.type = type;
	}

	@Override
	public Object stringValue(String value) {
		return null;
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		jsonLexer.skipCurrentString();
		return null;
	}

	@Override
	public Object intValue(String value) {
		return null;
	}

	@Override
	public Object floatValue(String value) {
		return null;
	}

	@Override
	public Object falseValue() {
		return null;
	}

	@Override
	public Object trueValue() {
		return null;
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		InitializationDevice device = INIT_MAP.get(type);
		return new ArrayDeserializerForCollection(device.newCollection(), device.getDeserializer());
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	private static class InitializationDevice {

		private MagicConstructor<?>	collection;

		private JSONDeserializer	deserializer;

		public InitializationDevice(MagicConstructor<?> collection, JSONDeserializer deserializer) {
			super();
			this.collection = collection;
			this.deserializer = deserializer;
		}

		@SuppressWarnings("unchecked")
		public Collection<Object> newCollection() {
			return (Collection<Object>) collection.newInstance();
		}

		public JSONDeserializer getDeserializer() {
			return deserializer;
		}

	}

	@Override
	public Object value(Integer value) {
		return null;
	}

	@Override
	public Object value(Double value) {
		return null;
	}

	@Override
	public Object value(Float value) {
		return null;
	}

	@Override
	public Object value(Long value) {
		return null;
	}

	@Override
	public Object value(Short value) {

		return null;
	}

	@Override
	public Object value(Byte value) {
		return null;
	}

	@Override
	public Object value(Boolean value) {
		return null;
	}

	@Override
	public Object value(Character value) {
		return null;
	}

	@Override
	public Object value(Date value) {
		return null;
	}

	@Override
	public Object value(BigDecimal value) {
		return null;
	}

	@Override
	public Object value(BigInteger value) {
		return null;
	}

	@Override
	public Object value(JSONBase value) {
		return null;
	}

	private static class InitializationDeviceBuilder implements ValueBuilder<Type, InitializationDevice> {

		@Override
		public InitializationDevice build(Type type) {
			Generic generic = Generic.getGeneric(type);

			Class<?> clazz = generic.getRealClass();

			MagicConstructor<?> collection;

			if (clazz == List.class || clazz == Collection.class) {
				collection = MagicConstructor.getMagicConstructor(ArrayList.class);
			} else if (clazz == Set.class) {
				collection = MagicConstructor.getMagicConstructor(HashSet.class);
			} else {
				collection = MagicConstructor.getMagicConstructor(clazz);
			}

			Type[] types = generic.getGenerics();

			JSONDeserializer deserializer;

			if (types.length == 1) {
				deserializer = JSONParser.getDeserializer(types[0]);
			} else {
				deserializer = DeserializerForObject.INSTANCE;
			}

			return new InitializationDevice(collection, deserializer);
		}

	}

}
