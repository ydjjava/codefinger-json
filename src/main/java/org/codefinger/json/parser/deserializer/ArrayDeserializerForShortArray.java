package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ArrayDeserializerForShortArray implements JSONArrayDeserializer {

	private static final short[]	DEFAULTCAPACITY_EMPTY_ELEMENTDATA	= new short[0];

	private short[]					elementData							= DEFAULTCAPACITY_EMPTY_ELEMENTDATA;

	private int						size								= 0;

	public ArrayDeserializerForShortArray() {
		super();
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public void addValue(Object value) {
	}

	@Override
	public void addStringValue(JSONLexer jsonLexer) {
		add(Short.parseShort(jsonLexer.getOriginalString()));
	}

	@Override
	public void addIntValue(String value) {
		add(Short.parseShort(value));
	}

	@Override
	public void addFloatValue(String value) {
		add(new BigDecimal(value).shortValue());
	}

	@Override
	public void addFalseValue() {
		add((short) 0);
	}

	@Override
	public void addTrueValue() {
		add((short) 1);
	}

	@Override
	public void addNullValue() {
	}

	@Override
	public Object getDeserializedValue() {
		return elementData.length == size ? elementData : Arrays.copyOf(elementData, size);
	}

	private void ensureCapacityInternal(int minCapacity) {
		if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
			minCapacity = Math.max(10, minCapacity);
		}
		ensureExplicitCapacity(minCapacity);
	}

	private void ensureExplicitCapacity(int minCapacity) {
		if (minCapacity - elementData.length > 0) {
			grow(minCapacity);
		}
	}

	private void grow(int minCapacity) {
		int oldCapacity = elementData.length;
		int newCapacity = oldCapacity + (oldCapacity >> 1);
		if (newCapacity - minCapacity < 0) {
			newCapacity = minCapacity;
		}
		elementData = Arrays.copyOf(elementData, newCapacity);
	}

	private void add(short value) {
		ensureCapacityInternal(size + 1);
		elementData[size++] = value;
	}

	@Override
	public void addValue(Integer value) {
		add(value.shortValue());
	}

	@Override
	public void addValue(Double value) {
		add(value.shortValue());
	}

	@Override
	public void addValue(Float value) {
		add(value.shortValue());
	}

	@Override
	public void addValue(Long value) {
		add(value.shortValue());
	}

	@Override
	public void addValue(Short value) {
		add(value.shortValue());
	}

	@Override
	public void addValue(Byte value) {
		add(value.shortValue());
	}

	@Override
	public void addValue(Boolean value) {
		add(value ? (short) 1 : 0);
	}

	@Override
	public void addValue(Character value) {
		add((short) value.charValue());
	}

	@Override
	public void addValue(Date value) {
		add((short) value.getTime());
	}

	@Override
	public void addValue(BigDecimal value) {
		add(value.shortValue());
	}

	@Override
	public void addValue(BigInteger value) {
		add(value.shortValue());
	}

	@Override
	public void addValue(JSONBase value) {
		add(value.shortValue());
	}

	@Override
	public void addValue(String value) {
		add(Byte.parseByte(value));
	}

	@Override
	public Object to(int[] value) {
		int length = value.length;
		short[] result = new short[length];
		for (int i = 0; i < length; i++) {
			result[i] = (short) value[i];
		}
		return result;

	}

	@Override
	public Object to(double[] value) {
		int length = value.length;
		short[] result = new short[length];
		for (int i = 0; i < length; i++) {
			result[i] = (short) value[i];
		}
		return result;
	}

	@Override
	public Object to(float[] value) {
		int length = value.length;
		short[] result = new short[length];
		for (int i = 0; i < length; i++) {
			result[i] = (short) value[i];
		}
		return result;
	}

	@Override
	public Object to(long[] value) {
		int length = value.length;
		short[] result = new short[length];
		for (int i = 0; i < length; i++) {
			result[i] = (short) value[i];
		}
		return result;
	}

	@Override
	public Object to(short[] value) {
		return Arrays.copyOf(value, value.length);
	}

	@Override
	public Object to(byte[] value) {
		int length = value.length;
		short[] result = new short[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i];
		}
		return result;
	}

	@Override
	public Object to(boolean[] value) {
		int length = value.length;
		short[] result = new short[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i] ? (short) 1 : 0;
		}
		return result;
	}

	@Override
	public Object to(char[] value) {
		int length = value.length;
		short[] result = new short[length];
		for (int i = 0; i < length; i++) {
			result[i] = (short) value[i];
		}
		return result;
	}

}
