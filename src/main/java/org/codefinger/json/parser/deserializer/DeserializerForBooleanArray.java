package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForBooleanArray implements JSONDeserializer {

	public static final DeserializerForBooleanArray	INSTANCE	= new DeserializerForBooleanArray();

	private DeserializerForBooleanArray() {

	}

	@Override
	public Object stringValue(String value) {
		return new boolean[] { Boolean.parseBoolean(value) };
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return new boolean[] { Boolean.parseBoolean(jsonLexer.getOriginalString()) };
	}

	@Override
	public Object intValue(String value) {
		return new boolean[] { Integer.parseInt(value) != 0 };
	}

	@Override
	public Object floatValue(String value) {
		return new boolean[] { new BigDecimal(value).intValue() != 0 };
	}

	@Override
	public Object falseValue() {
		return new boolean[] { false };
	}

	@Override
	public Object trueValue() {
		return new boolean[] { true };
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForBooleanArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return new boolean[] { value != 0 };
	}

	@Override
	public Object value(Double value) {
		return new boolean[] { value != 0 };
	}

	@Override
	public Object value(Float value) {
		return new boolean[] { value != 0 };
	}

	@Override
	public Object value(Long value) {
		return new boolean[] { value != 0 };
	}

	@Override
	public Object value(Short value) {
		return new boolean[] { value != 0 };
	}

	@Override
	public Object value(Byte value) {
		return new boolean[] { value != 0 };
	}

	@Override
	public Object value(Boolean value) {
		return new boolean[] { value };
	}

	@Override
	public Object value(Character value) {
		return new boolean[] { value != 0 };
	}

	@Override
	public Object value(Date value) {
		return new boolean[] { value.getTime() != 0 };
	}

	@Override
	public Object value(BigDecimal value) {
		return new boolean[] { value.intValue() != 0 };
	}

	@Override
	public Object value(BigInteger value) {
		return new boolean[] { value.intValue() != 0 };
	}

	@Override
	public Object value(JSONBase value) {
		return new boolean[] { value.intValue() != 0 };
	}

}
