package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForJSONArray implements JSONDeserializer {

	public static final DeserializerForJSONArray	INSTANCE	= new DeserializerForJSONArray();

	private DeserializerForJSONArray() {

	}

	@Override
	public Object stringValue(String value) {
		return null;
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		jsonLexer.skipCurrentString();
		return null;
	}

	@Override
	public Object intValue(String value) {
		return null;
	}

	@Override
	public Object floatValue(String value) {
		return null;
	}

	@Override
	public Object falseValue() {
		return null;
	}

	@Override
	public Object trueValue() {
		return null;
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForJSONArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return null;
	}

	@Override
	public Object value(Double value) {
		return null;
	}

	@Override
	public Object value(Float value) {
		return null;
	}

	@Override
	public Object value(Long value) {
		return null;
	}

	@Override
	public Object value(Short value) {
		return null;
	}

	@Override
	public Object value(Byte value) {
		return null;
	}

	@Override
	public Object value(Boolean value) {
		return null;
	}

	@Override
	public Object value(Character value) {
		return null;
	}

	@Override
	public Object value(Date value) {
		return null;
	}

	@Override
	public Object value(BigDecimal value) {
		return null;
	}

	@Override
	public Object value(BigInteger value) {
		return null;
	}

	@Override
	public Object value(JSONBase value) {
		return null;
	}

}
