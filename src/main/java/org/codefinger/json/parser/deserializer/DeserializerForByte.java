package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForByte implements JSONDeserializer {

	public static final DeserializerForByte	INSTANCE	= new DeserializerForByte();

	private DeserializerForByte() {

	}

	@Override
	public Object stringValue(String value) {
		return Byte.valueOf(value);
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return Byte.valueOf(jsonLexer.getOriginalString());
	}

	@Override
	public Object intValue(String value) {
		return Byte.valueOf(value);
	}

	@Override
	public Object floatValue(String value) {
		return Byte.valueOf(new BigDecimal(value).byteValue());
	}

	@Override
	public Object falseValue() {
		return Byte.valueOf((byte) 0);
	}

	@Override
	public Object trueValue() {
		return Byte.valueOf((byte) 1);
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return value.byteValue();
	}

	@Override
	public Object value(Double value) {
		return value.byteValue();
	}

	@Override
	public Object value(Float value) {
		return value.byteValue();
	}

	@Override
	public Object value(Long value) {
		return value.byteValue();
	}

	@Override
	public Object value(Short value) {
		return value.byteValue();
	}

	@Override
	public Object value(Byte value) {
		return value;
	}

	@Override
	public Object value(Boolean value) {
		return value ? (byte) 1 : 0;
	}

	@Override
	public Object value(Character value) {
		return (byte) value.charValue();
	}

	@Override
	public Object value(Date value) {
		return (byte) value.getTime();
	}

	@Override
	public Object value(BigDecimal value) {
		return value.byteValue();
	}

	@Override
	public Object value(BigInteger value) {
		return value.byteValue();
	}

	@Override
	public Object value(JSONBase value) {
		return value.byteValue();
	}

}
