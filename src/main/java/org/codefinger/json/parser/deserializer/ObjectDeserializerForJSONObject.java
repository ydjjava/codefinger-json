package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.JSONObject;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ObjectDeserializerForJSONObject implements JSONObjectDeserializer {

	private JSONObject	jsonObject	= new JSONObject();

	private String		fieldName;

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForJSONArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return new ObjectDeserializerForJSONObject();
	}

	@Override
	public void setFieldName(JSONLexer jsonLexer) {
		this.fieldName = jsonLexer.getJSONString();
	}

	@Override
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	@Override
	public void setValue(Object value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setStringValue(JSONLexer jsonLexer) {
		jsonObject.put(fieldName, jsonLexer.getJSONString());
	}

	@Override
	public void setIntValue(String value) {
		jsonObject.put(fieldName, new BigDecimal(value));
	}

	@Override
	public void setFloatValue(String value) {
		jsonObject.put(fieldName, new BigDecimal(value));
	}

	@Override
	public void setFalseValue() {
		jsonObject.put(fieldName, false);
	}

	@Override
	public void setTrueValue() {
		jsonObject.put(fieldName, true);
	}

	@Override
	public Object getDeserializedValue() {
		return jsonObject;
	}

	@Override
	public void setValue(Integer value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(Double value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(Float value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(Long value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(Short value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(Byte value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(Boolean value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(Character value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(Date value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(BigDecimal value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(BigInteger value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(JSONBase value) {
		jsonObject.put(fieldName, value);
	}

	@Override
	public void setValue(String value) {
		jsonObject.put(fieldName, value);
	}

}
