package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ArrayDeserializerForByteArray implements JSONArrayDeserializer {

	private static final byte[]	DEFAULTCAPACITY_EMPTY_ELEMENTDATA	= new byte[0];

	private byte[]				elementData							= DEFAULTCAPACITY_EMPTY_ELEMENTDATA;

	private int					size								= 0;

	public ArrayDeserializerForByteArray() {
		super();
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public void addValue(Object value) {
	}

	@Override
	public void addStringValue(JSONLexer jsonLexer) {
		add(Byte.parseByte(jsonLexer.getOriginalString()));
	}

	@Override
	public void addIntValue(String value) {
		add(Byte.parseByte(value));
	}

	@Override
	public void addFloatValue(String value) {
		add(new BigDecimal(value).byteValue());
	}

	@Override
	public void addFalseValue() {
		add((byte) 0);
	}

	@Override
	public void addTrueValue() {
		add((byte) 1);
	}

	@Override
	public void addNullValue() {
	}

	@Override
	public Object getDeserializedValue() {
		return elementData.length == size ? elementData : Arrays.copyOf(elementData, size);
	}

	private void ensureCapacityInternal(int minCapacity) {
		if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
			minCapacity = Math.max(10, minCapacity);
		}
		ensureExplicitCapacity(minCapacity);
	}

	private void ensureExplicitCapacity(int minCapacity) {
		if (minCapacity - elementData.length > 0) {
			grow(minCapacity);
		}
	}

	private void grow(int minCapacity) {
		int oldCapacity = elementData.length;
		int newCapacity = oldCapacity + (oldCapacity >> 1);
		if (newCapacity - minCapacity < 0) {
			newCapacity = minCapacity;
		}
		elementData = Arrays.copyOf(elementData, newCapacity);
	}

	private void add(byte value) {
		ensureCapacityInternal(size + 1);
		elementData[size++] = value;
	}

	@Override
	public void addValue(Integer value) {
		add(value.byteValue());
	}

	@Override
	public void addValue(Double value) {
		add(value.byteValue());
	}

	@Override
	public void addValue(Float value) {
		add(value.byteValue());
	}

	@Override
	public void addValue(Long value) {
		add(value.byteValue());
	}

	@Override
	public void addValue(Short value) {
		add(value.byteValue());
	}

	@Override
	public void addValue(Byte value) {
		add(value.byteValue());
	}

	@Override
	public void addValue(Boolean value) {
		add(value ? (byte) 1 : 0);
	}

	@Override
	public void addValue(Character value) {
		add((byte) value.charValue());
	}

	@Override
	public void addValue(Date value) {
		add((byte) value.getTime());
	}

	@Override
	public void addValue(BigDecimal value) {
		add(value.byteValue());
	}

	@Override
	public void addValue(BigInteger value) {
		add(value.byteValue());
	}

	@Override
	public void addValue(JSONBase value) {
		add(value.byteValue());
	}

	@Override
	public void addValue(String value) {
		add(Byte.parseByte(value));
	}

	@Override
	public Object to(int[] value) {
		int length = value.length;
		byte[] result = new byte[length];
		for (int i = 0; i < length; i++) {
			result[i] = (byte) value[i];
		}
		return result;
	}

	@Override
	public Object to(double[] value) {
		int length = value.length;
		byte[] result = new byte[length];
		for (int i = 0; i < length; i++) {
			result[i] = (byte) value[i];
		}
		return result;
	}

	@Override
	public Object to(float[] value) {
		int length = value.length;
		byte[] result = new byte[length];
		for (int i = 0; i < length; i++) {
			result[i] = (byte) value[i];
		}
		return result;
	}

	@Override
	public Object to(long[] value) {
		int length = value.length;
		byte[] result = new byte[length];
		for (int i = 0; i < length; i++) {
			result[i] = (byte) value[i];
		}
		return result;
	}

	@Override
	public Object to(short[] value) {
		int length = value.length;
		byte[] result = new byte[length];
		for (int i = 0; i < length; i++) {
			result[i] = (byte) value[i];
		}
		return result;
	}

	@Override
	public Object to(byte[] value) {
		return Arrays.copyOf(value, value.length);
	}

	@Override
	public Object to(boolean[] value) {
		int length = value.length;
		byte[] result = new byte[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i] ? (byte) 1 : 0;
		}
		return result;
	}

	@Override
	public Object to(char[] value) {
		int length = value.length;
		byte[] result = new byte[length];
		for (int i = 0; i < length; i++) {
			result[i] = (byte) value[i];
		}
		return result;
	}

}
