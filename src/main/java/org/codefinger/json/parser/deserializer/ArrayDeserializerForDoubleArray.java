package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ArrayDeserializerForDoubleArray implements JSONArrayDeserializer {

	private static final double[]	DEFAULTCAPACITY_EMPTY_ELEMENTDATA	= new double[0];

	private double[]				elementData							= DEFAULTCAPACITY_EMPTY_ELEMENTDATA;

	private int						size								= 0;

	public ArrayDeserializerForDoubleArray() {
		super();
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public void addValue(Object value) {
	}

	@Override
	public void addStringValue(JSONLexer jsonLexer) {
		add(Double.parseDouble(jsonLexer.getOriginalString()));
	}

	@Override
	public void addIntValue(String value) {
		add(Double.parseDouble(value));
	}

	@Override
	public void addFloatValue(String value) {
		add(Double.parseDouble(value));
	}

	@Override
	public void addFalseValue() {
		add(0.0);
	}

	@Override
	public void addTrueValue() {
		add(1.0);
	}

	@Override
	public void addNullValue() {
	}

	@Override
	public Object getDeserializedValue() {
		return elementData.length == size ? elementData : Arrays.copyOf(elementData, size);
	}

	private void ensureCapacityInternal(int minCapacity) {
		if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
			minCapacity = Math.max(10, minCapacity);
		}
		ensureExplicitCapacity(minCapacity);
	}

	private void ensureExplicitCapacity(int minCapacity) {
		if (minCapacity - elementData.length > 0) {
			grow(minCapacity);
		}
	}

	private void grow(int minCapacity) {
		int oldCapacity = elementData.length;
		int newCapacity = oldCapacity + (oldCapacity >> 1);
		if (newCapacity - minCapacity < 0) {
			newCapacity = minCapacity;
		}
		elementData = Arrays.copyOf(elementData, newCapacity);
	}

	private void add(double value) {
		ensureCapacityInternal(size + 1);
		elementData[size++] = value;
	}

	@Override
	public void addValue(Integer value) {
		add(value);
	}

	@Override
	public void addValue(Double value) {
		add(value);
	}

	@Override
	public void addValue(Float value) {
		add(value);
	}

	@Override
	public void addValue(Long value) {
		add(value);

	}

	@Override
	public void addValue(Short value) {
		add(value);

	}

	@Override
	public void addValue(Byte value) {
		add(value);

	}

	@Override
	public void addValue(Boolean value) {
		add(value ? 1 : 0);

	}

	@Override
	public void addValue(Character value) {
		add(value);
	}

	@Override
	public void addValue(Date value) {
		add(value.getTime());
	}

	@Override
	public void addValue(BigDecimal value) {
		add(value.doubleValue());
	}

	@Override
	public void addValue(BigInteger value) {
		add(value.doubleValue());
	}

	@Override
	public void addValue(JSONBase value) {
		add(value.doubleValue());
	}

	@Override
	public void addValue(String value) {
		add(Double.parseDouble(value));
	}

	@Override
	public Object to(int[] value) {
		int length = value.length;
		double[] result = new double[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i];
		}
		return result;
	}

	@Override
	public Object to(double[] value) {
		return Arrays.copyOf(value, value.length);
	}

	@Override
	public Object to(float[] value) {
		int length = value.length;
		double[] result = new double[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i];
		}
		return result;
	}

	@Override
	public Object to(long[] value) {
		int length = value.length;
		double[] result = new double[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i];
		}
		return result;
	}

	@Override
	public Object to(short[] value) {
		int length = value.length;
		double[] result = new double[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i];
		}
		return result;
	}

	@Override
	public Object to(byte[] value) {
		int length = value.length;
		double[] result = new double[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i];
		}
		return result;
	}

	@Override
	public Object to(boolean[] value) {
		int length = value.length;
		double[] result = new double[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i] ? 1 : 0;
		}
		return result;
	}

	@Override
	public Object to(char[] value) {
		int length = value.length;
		double[] result = new double[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i];
		}
		return result;
	}

}
