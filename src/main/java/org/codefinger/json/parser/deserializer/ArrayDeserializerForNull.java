package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ArrayDeserializerForNull implements JSONArrayDeserializer {

	public static final ArrayDeserializerForNull	INSTANCE	= new ArrayDeserializerForNull();

	private ArrayDeserializerForNull() {

	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public void addValue(Object value) {

	}

	@Override
	public void addStringValue(JSONLexer jsonLexer) {
		jsonLexer.skipCurrentString();
	}

	@Override
	public void addIntValue(String value) {

	}

	@Override
	public void addFloatValue(String value) {

	}

	@Override
	public void addFalseValue() {

	}

	@Override
	public void addTrueValue() {

	}

	@Override
	public void addNullValue() {

	}

	@Override
	public Object getDeserializedValue() {
		return null;
	}

	@Override
	public void addValue(Integer value) {

	}

	@Override
	public void addValue(Double value) {

	}

	@Override
	public void addValue(Float value) {

	}

	@Override
	public void addValue(Long value) {

	}

	@Override
	public void addValue(Short value) {

	}

	@Override
	public void addValue(Byte value) {

	}

	@Override
	public void addValue(Boolean value) {

	}

	@Override
	public void addValue(Character value) {

	}

	@Override
	public void addValue(Date value) {

	}

	@Override
	public void addValue(BigDecimal value) {

	}

	@Override
	public void addValue(BigInteger value) {

	}

	@Override
	public void addValue(JSONBase value) {

	}

	@Override
	public void addValue(String value) {

	}

	@Override
	public Object to(int[] value) {
		return null;
	}

	@Override
	public Object to(double[] value) {
		return null;
	}

	@Override
	public Object to(float[] value) {
		return null;
	}

	@Override
	public Object to(long[] value) {
		return null;
	}

	@Override
	public Object to(short[] value) {
		return null;
	}

	@Override
	public Object to(byte[] value) {
		return null;
	}

	@Override
	public Object to(boolean[] value) {
		return null;
	}

	@Override
	public Object to(char[] value) {
		return null;
	}

}
