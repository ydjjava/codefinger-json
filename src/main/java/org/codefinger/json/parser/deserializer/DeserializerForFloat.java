package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForFloat implements JSONDeserializer {

	public static final DeserializerForFloat	INSTANCE	= new DeserializerForFloat();

	private DeserializerForFloat() {

	}

	@Override
	public Object stringValue(String value) {
		return Float.valueOf(value);
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return Float.valueOf(jsonLexer.getOriginalString());
	}

	@Override
	public Object intValue(String value) {
		return Float.valueOf(value);
	}

	@Override
	public Object floatValue(String value) {
		return Float.valueOf(value);
	}

	@Override
	public Object falseValue() {
		return Float.valueOf(0);
	}

	@Override
	public Object trueValue() {
		return Float.valueOf(1);
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return value.floatValue();
	}

	@Override
	public Object value(Double value) {
		return value.floatValue();
	}

	@Override
	public Object value(Float value) {
		return value;
	}

	@Override
	public Object value(Long value) {
		return value.floatValue();
	}

	@Override
	public Object value(Short value) {
		return value.floatValue();
	}

	@Override
	public Object value(Byte value) {
		return value.floatValue();
	}

	@Override
	public Object value(Boolean value) {
		return value ? 1F : 0F;
	}

	@Override
	public Object value(Character value) {
		return (float) value.charValue();
	}

	@Override
	public Object value(Date value) {
		return (float) value.getTime();
	}

	@Override
	public Object value(BigDecimal value) {
		return value.floatValue();
	}

	@Override
	public Object value(BigInteger value) {
		return value.floatValue();
	}

	@Override
	public Object value(JSONBase value) {
		return value.floatValue();
	}

}
