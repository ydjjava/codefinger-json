package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForLongArray implements JSONDeserializer {

	public static final DeserializerForLongArray	INSTANCE	= new DeserializerForLongArray();

	private DeserializerForLongArray() {

	}

	@Override
	public Object stringValue(String value) {
		return new long[] { Long.parseLong(value) };
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return new long[] { Long.parseLong(jsonLexer.getOriginalString()) };
	}

	@Override
	public Object intValue(String value) {
		return new long[] { Long.parseLong(value) };
	}

	@Override
	public Object floatValue(String value) {
		return new long[] { new BigDecimal(value).longValue() };
	}

	@Override
	public Object falseValue() {
		return new long[] { 0 };
	}

	@Override
	public Object trueValue() {
		return new long[] { 1 };
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForLongArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return new long[] { value.longValue() };
	}

	@Override
	public Object value(Double value) {
		return new long[] { value.longValue() };
	}

	@Override
	public Object value(Float value) {
		return new long[] { value.longValue() };
	}

	@Override
	public Object value(Long value) {
		return new long[] { value };
	}

	@Override
	public Object value(Short value) {
		return new long[] { value.longValue() };
	}

	@Override
	public Object value(Byte value) {
		return new long[] { value.longValue() };
	}

	@Override
	public Object value(Boolean value) {
		return new long[] { value ? 1L : 0L };
	}

	@Override
	public Object value(Character value) {
		return new long[] { value.charValue() };
	}

	@Override
	public Object value(Date value) {
		return new long[] { value.getTime() };
	}

	@Override
	public Object value(BigDecimal value) {
		return new long[] { value.longValue() };
	}

	@Override
	public Object value(BigInteger value) {
		return new long[] { value.longValue() };
	}

	@Override
	public Object value(JSONBase value) {
		return new long[] { value.longValue() };
	}

}
