package org.codefinger.json.parser.deserializer;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Map;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ObjectDeserializerForMap implements JSONObjectDeserializer {

	private Map<Object, Object>	map;

	private JSONDeserializer	keyDeserializer;

	private JSONDeserializer	valueDeserializer;

	private String				fieldName;

	public ObjectDeserializerForMap(Map<Object, Object> map, JSONDeserializer keyDeserializer, JSONDeserializer valueDeserializer, Type type) {
		super();
		this.map = map;
		this.keyDeserializer = keyDeserializer;
		this.valueDeserializer = valueDeserializer;
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return valueDeserializer.createArrayDeserializer();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return valueDeserializer.createObjectDeserializer();
	}

	@Override
	public void setFieldName(JSONLexer jsonLexer) {
		this.fieldName = jsonLexer.getJSONString();
	}

	@Override
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	@Override
	public void setValue(Object value) {
		map.put(keyDeserializer.stringValue(fieldName), value);
	}

	@Override
	public void setStringValue(JSONLexer jsonLexer) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.stringValue(jsonLexer));
	}

	@Override
	public void setIntValue(String value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.intValue(value));
	}

	@Override
	public void setFloatValue(String value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.floatValue(value));
	}

	@Override
	public void setFalseValue() {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.falseValue());
	}

	@Override
	public void setTrueValue() {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.trueValue());
	}

	@Override
	public Object getDeserializedValue() {
		return map;
	}

	@Override
	public void setValue(Integer value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(Double value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(Float value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(Long value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(Short value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(Byte value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(Boolean value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(Character value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(Date value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(BigDecimal value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(BigInteger value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(JSONBase value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.value(value));
	}

	@Override
	public void setValue(String value) {
		map.put(keyDeserializer.stringValue(fieldName), valueDeserializer.stringValue(value));
	}

}
