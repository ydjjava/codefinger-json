package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;
import org.codefinger.json.util.Base64;

public class DeserializerForByteArray implements JSONDeserializer {

	public static final DeserializerForByteArray	INSTANCE	= new DeserializerForByteArray();

	private DeserializerForByteArray() {

	}

	@Override
	public Object stringValue(String value) {
		return Base64.decodeFast(value);
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return Base64.decodeFast(jsonLexer.getOriginalCharArray());
	}

	@Override
	public Object intValue(String value) {
		return value.getBytes();
	}

	@Override
	public Object floatValue(String value) {
		return value.getBytes();
	}

	@Override
	public Object falseValue() {
		return new byte[] { 102, 97, 108, 115, 101 };
	}

	@Override
	public Object trueValue() {
		return new byte[] { 116, 114, 117, 101 };
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForByteArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return new byte[] { value.byteValue() };
	}

	@Override
	public Object value(Double value) {
		return new byte[] { value.byteValue() };
	}

	@Override
	public Object value(Float value) {
		return new byte[] { value.byteValue() };
	}

	@Override
	public Object value(Long value) {
		return new byte[] { value.byteValue() };
	}

	@Override
	public Object value(Short value) {
		return new byte[] { value.byteValue() };
	}

	@Override
	public Object value(Byte value) {
		return new byte[] { value.byteValue() };
	}

	@Override
	public Object value(Boolean value) {
		return new byte[] { value ? (byte) 1 : 0 };
	}

	@Override
	public Object value(Character value) {
		return new byte[] { (byte) value.charValue() };
	}

	@Override
	public Object value(Date value) {
		return new byte[] { (byte) value.getTime() };
	}

	@Override
	public Object value(BigDecimal value) {
		return new byte[] { value.byteValue() };
	}

	@Override
	public Object value(BigInteger value) {
		return new byte[] { value.byteValue() };
	}

	@Override
	public Object value(JSONBase value) {
		return new byte[] { value.byteValue() };
	}
}
