package org.codefinger.json.parser;

public interface JSONConverter {

	Object convert(Object value, JSONDeserializer deserializer);

	Object convert(Object value, JSONObjectDeserializer objectDeserializer);

	Object convert(Object value, JSONArrayDeserializer arrayDeserializer);

	void setItemValue(Object value, JSONObjectDeserializer objectDeserializer);

	void addItemValue(Object value, JSONArrayDeserializer arrayDeserializer);

	JSONConverterType jsonConverterType();
}
