package org.codefinger.json.parser;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;

public interface JSONArrayDeserializer {

	JSONArrayDeserializer createArrayDeserializer();

	JSONObjectDeserializer createObjectDeserializer();

	void addValue(Object value);

	void addStringValue(JSONLexer jsonLexer);

	void addIntValue(String value);

	void addFloatValue(String value);

	void addFalseValue();

	void addTrueValue();

	void addNullValue();

	Object getDeserializedValue();

	void addValue(Integer value);

	void addValue(Double value);

	void addValue(Float value);

	void addValue(Long value);

	void addValue(Short value);

	void addValue(Byte value);

	void addValue(Boolean value);

	void addValue(Character value);

	void addValue(Date value);

	void addValue(BigDecimal value);

	void addValue(BigInteger value);

	void addValue(JSONBase value);

	void addValue(String value);

	Object to(int[] value);

	Object to(double[] value);

	Object to(float[] value);

	Object to(long[] value);

	Object to(short[] value);

	Object to(byte[] value);

	Object to(boolean[] value);

	Object to(char[] value);

}
