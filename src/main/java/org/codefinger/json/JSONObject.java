package org.codefinger.json;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.codefinger.json.parser.JSONParser;
import org.codefinger.json.util.type.TypeUtil;

public final class JSONObject extends HashMap<String, Object> implements JSON {

	private static final long	serialVersionUID	= 3377926157594652428L;

	public JSONObject() {
		super();
	}

	public JSONObject(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
	}

	public JSONObject(int initialCapacity) {
		super(initialCapacity);
	}

	public JSONObject(Map<? extends String, ? extends Object> m) {
		super(m);
	}

	public Integer getInt(String key) {
		return TypeUtil.castToInt(get(key));
	}

	public Short getShort(String key) {
		return TypeUtil.castToShort(get(key));
	}

	public Double getDouble(String key) {
		return TypeUtil.castToDouble(get(key));
	}

	public Float getFloat(String key) {
		return TypeUtil.castToFloat(get(key));
	}

	public Character getChar(String key) {
		return TypeUtil.castToChar(get(key));
	}

	public Long getLong(String key) {
		return TypeUtil.castToLong(get(key));
	}

	public Byte getByte(String key) {
		return TypeUtil.castToByte(get(key));
	}

	public Boolean getBoolean(String key) {
		return TypeUtil.castToBoolean(get(key));
	}

	public String getString(String key) {
		return TypeUtil.castToString(get(key));
	}

	public BigDecimal getBigDecimal(String key) {
		return TypeUtil.castToBigDecimal(get(key));
	}

	public BigInteger getBigInteger(String key) {
		return TypeUtil.castToBigInteger(get(key));
	}

	public Date getDate(String key) {
		return TypeUtil.castToDate(get(key));
	}

	@SuppressWarnings("unchecked")
	public <T> T toJavaObject(Class<T> clazz) {
		return (T) JSONParser.parseTo(this, clazz);
	}

	@SuppressWarnings("unchecked")
	public <T> T toJavaObject(Type type) {
		return (T) JSONParser.parseTo(this, type);
	}

	@Override
	public JSONType jsonType() {
		return JSONType.Object;
	}

	@Override
	public String toString() {
		return JSONUtil.toJSONString(this);
	}

}
