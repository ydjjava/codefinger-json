package org.codefinger.json;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.TypeUtil;

public final class JSONBase implements JSON {

	private Object		value;

	private JSONType	jsonType;

	public JSONBase(boolean value) {
		this.value = value;
		this.jsonType = JSONType.Boolean;
	}

	public JSONBase(Number value) {
		if (value == null) {
			throw new NullPointerException("Value can not be null.");
		}
		this.value = value;
		this.jsonType = JSONType.Number;
	}

	public JSONBase(String value) {
		if (value == null) {
			throw new NullPointerException("Value can not be null.");
		}
		this.value = value;
		this.jsonType = JSONType.String;
	}

	public JSONBase(Date value) {
		if (value == null) {
			throw new NullPointerException("Value can not be null.");
		}
		this.value = value;
		this.jsonType = JSONType.Date;
	}

	public Integer intValue() {
		return TypeUtil.castToInt(value);
	}

	public Short shortValue() {
		return TypeUtil.castToShort(value);
	}

	public Double doubleValue() {
		return TypeUtil.castToDouble(value);
	}

	public Float floatValue() {
		return TypeUtil.castToFloat(value);
	}

	public Character charValue() {
		return TypeUtil.castToChar(value);
	}

	public Long longValue() {
		return TypeUtil.castToLong(value);
	}

	public Byte byteValue() {
		return TypeUtil.castToByte(value);
	}

	public Boolean booleanValue() {
		return TypeUtil.castToBoolean(value);
	}

	public String stringValue() {
		return TypeUtil.castToString(value);
	}

	public BigDecimal bigDecimalValue() {
		return TypeUtil.castToBigDecimal(value);
	}

	public BigInteger bigIntegerValue() {
		return TypeUtil.castToBigInteger(value);
	}

	public Date dateValue() {
		return TypeUtil.castToDate(value);
	}

	public String toJSONString() {
		return JSONUtil.toJSONString(value);
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return value.equals(obj);
	}

	@Override
	public String toString() {
		return value.toString();
	}

	@Override
	public JSONType jsonType() {
		return jsonType;
	}

}
